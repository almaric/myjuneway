FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre++-dev zlib1g-dev
RUN wget https://nginx.org/download/nginx-1.20.0.tar.gz && tar xvfz nginx-1.20.0.tar.gz && cd nginx-1.20.0 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]